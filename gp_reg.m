function [model] = gp_reg()
%% Gaussian Process (GP) regression
% N(f|mu,Sigma)
% mu = mu(xs) + Ks'inv(K)(f-mu(x))
% Sigma = Kss - Ks'inv(K)Ks

clear all; close all; 
rng('default');


%% generate noise-less training data
%Xtrain = (-5:0.8:5)';%[-4, -3, -2, -1, 1]';
Xtrain = [4,-4,-1.5,1,5,-5,2.5,-2.5]';
ftrain =-(Xtrain+1).^2; % exp (sin(Xtrain))

%% generate data
L = 2;%Xtrain(1)-Xtrain(2);
xs = (-5:0.01:5)'; %test data
ns = length(xs);
keps = 0.01; %error

%define mean and kernel functions
%muFn = @(x) 0*x(:).^2;
Kfn = @(x,z) 100*exp(-sq_dist(x'/L,z'/L)/2);

%% plot sampled functions from the prior
% figure; hold on
% for i=1:3
%   model = struct('mu', muFn(xs), 'Sigma',  Kfn(xs, xs) + 1e-15*eye(size(xs, 1)));
%   fs = gauss_sample(model, 1);
%   plot(xs, fs, 'k-', 'linewidth', 2)
% end
% title('samples from GP prior');



%% compute posterior predictive
K = Kfn(Xtrain, Xtrain); % K
Ks = Kfn(Xtrain, xs); %K_*
Kss = Kfn(xs, xs) + keps*eye(length(xs)); % K_** (keps is essential!)
Ki = inv(K); %O(Ntrain^3)
postMu = mean(ftrain) + Ks'*Ki*(ftrain - mean(ftrain));%muFn(xs) + Ks'*Ki*(ftrain - muFn(Xtrain))
postCov = Kss - Ks'*Ki*Ks;


%% plot samples from posterior predictive
figure; hold on
% plot marginal posterior variance as gray band
mu = postMu(:);
S2 = diag(postCov);
f = [mu+2.96*sqrt(S2);flip(mu-2.96*sqrt(S2),1)];
fill([xs; flip(xs,1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);

%  for i=1:5
%    model = struct('mu', postMu(:), 'Sigma', postCov);
%    fs = gauss_sample(model, 1);
   plot(xs, postMu, 'b-', 'linewidth', 1)
    plot(Xtrain, ftrain, 'r.', 'markersize', 12, 'linewidth', 3);
    plot(xs,-(xs+1).^2,'m-');%-(xs+1).^2
%  end
title('samples from GP posterior');

end


function S = gauss_sample(model, n)
% Returns n samples from a multivariate Gaussian distribution
% S = AZ + mu

mu = model.mu;
Sigma = model.Sigma;

A = chol(Sigma, 'lower');
Z = randn(length(mu), n);
S = bsxfun(@plus, mu(:), A*Z)';
end
