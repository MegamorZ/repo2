%% 2D data
%L = 0.5;
Xtest = (-5:0.05:5)'; %test data
Ytest = exp (sin(Xtest));

Xtrain = (-5:0.3:5)';%[-4, -3, -2, -1, 1]';
Y = exp (sin(Xtrain));



%% 3D data
%xtrain
% x=-5:1:5;
% y=x;
% [X1train, X2train]= meshgrid (x,y);
% Xtrain=[X1train(:),X2train(:)];
% 
% %xtest
% x=-5:0.1:5;
% y=x;
% [X1test, X2test]= meshgrid (x,y);
% Xtest=[X1test(:),X2test(:)];
% 
% %ytrain
% for i=1:size(Xtrain,1)
%     Y(i)=-sin(Xtrain(i,1))*cos(Xtrain(i,2));
% end
% %ytest
% for i=1:size(Xtest,1)
%     Ytest(i)=-sin(Xtest(i,1))*cos(Xtest(i,2));
% end

%% 