function [RMSE]=GPR(Xtrain,Y,Xtest,Ytest,Lambda,S,r)

% dim Xtrain: # observaciones x # dimensiones
% dim Xtest: # prediocciones x # dimensiones
% dim Y: 1 x # Observaciones
% dim Xtrain: 1 x # predicciones

%tic
%% calculo de K
% r=0.001;
% Lambda=0.680907456071745;
% S=459254.564892358;
Lambda
S
K=Kernel (Xtrain,Xtrain,Lambda,S);

%% calculo K*

Ke=Kernel (Xtrain,Xtest,Lambda,S);
L=chol(K+r*eye(size(Xtrain,1))); %cholesky decomposition
alpha=inv(L)'*inv(L)*Y';
muF=Ke'*alpha; % mean function

%muF = Ke'/K*Y';% para 3D

%% Varianza 1
% L=chol(K+r*eye(size(Xtrain,1))); %cholesky decomposition
% v =inv(L)*Ke;
% Kee=Kernel (Xtest,Xtest,Lambda,S);
% varF1= diag(Kee)'- sum(v.^2);% variance funcrion
% %toc
% Varianza 2
Kee=Kernel (Xtest,Xtest,Lambda,S);
varF = diag(Kee - Ke'/K*Ke);
%% RMSE

RMSE=sqrt(sum((Ytest-muF').^2)/length(Ytest))

%% plot samples 2D
figure; hold on
% plot marginal posterior variance as gray band
% mu = postMu(:);
% S2 = diag(postCov);
   f = [muF+2.96*sqrt(varF);flip(muF-2.96*sqrt(varF),1)];
    fill([Xtest; flip(Xtest,1)], f, [7 7 7]/8, 'EdgeColor', [7 7 7]/8);

% %  for i=1:5
% %    model = struct('mu', postMu(:), 'Sigma', postCov);
% %    fs = gauss_sample(model, 1);
   plot(Xtest, muF, 'b-', 'linewidth', 1)
    plot(Xtrain, Y, 'r.', 'markersize', 12, 'linewidth', 3);
    plot(Xtest,exp(sin(Xtest)),'m-')
% %  end
%title('samples from GP posterior');

%% plot 3D case
% subplot(1,3,1),scatter3(Xtest(:,1),Xtest(:,2),Ytest,10,Ytest)
% subplot(1,3,2),scatter3(Xtest(:,1),Xtest(:,2),muF,10,muF)
% subplot(1,3,3),

% scatter3(Xtest(:,1),Xtest(:,2),(muF-Ytest'),10,(muF-Ytest'))

% figure()
%  scatter(muF,Ytest,'.')
% % 
%  figure()
% scatter3(Xtest(:,1),Xtest(:,2),muF,10,muF)
% hold on
% scatter3(Xtrain(:,1),Xtrain(:,2),Y,'kX')

end

%% function Kernel
function [k]=Kernel (A,B,L,S)

K=zeros(size(A,1),size(B,1));
for i=1:size(A,1)
    for j=1:size(B,1)
        k(i,j)= S^2*exp(-1/L*(norm(B(j,:)-A(i,:))^2));
    end
end
end