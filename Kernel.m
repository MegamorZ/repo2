%% function Kernel
function [k]=Kernel (A,B,L,S)

K=zeros(size(A,1),size(B,1));
for i=1:size(A,1)
    for j=1:size(B,1)
        k(i,j)= S^2*exp(-1/L*(norm(B(j,:)-A(i,:))^2));
    end
end
end